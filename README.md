# A simple Monte Carlo simulation of the [Moran process on graphs](https://geodynapp.wordpress.com/evolutionary-dynamics-on-networks/moran-model/) #

This bucket contains an HTML/JS/CSS implementation of the Moran process on graphs (Of course, the results might not usable because of the PRNG used). Feel free to use it.


[![CCIcon](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)